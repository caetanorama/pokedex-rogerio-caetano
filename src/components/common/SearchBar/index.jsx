import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
  Grid,
  TextField,
 } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { reset, searchPokemons, allPokemons } from '../../../helpers/actions'

import './SearchBar.sass'

class SearchBar extends Component {
    constructor(props) {
      super(props);
      this.props = props;
      this.state = {
        searchText: '',
      };

      this.handleKeyDown = this.handleKeyDown.bind(this);
      this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
      if (!e.target.value) this.props.handlePokemons(0)
      this.setState({
        searchText: e.target.value
      })
    }

    handleClick(e) {
      console.log('Click ->', e);
    }

    handleKeyDown(e) {
      if (e.key === 'Enter') {
        const pokemonName = this.state.searchText;
        if (pokemonName) {
          this.props
            .searchPokemons(pokemonName.toLocaleLowerCase());
        } else {
          this.props.handlePokemons(0)
        }
      }
    }

  render() {
    return (
      <Grid item xs={12} sm={4}>
        <Grid
          container
          spacing={1}
          alignItems='center'
          justify='center'
          className='search-tool'>
          <Grid item xs={1}>
            <SearchIcon />
          </Grid>
          <Grid item xs={11}>
            <TextField
              type="search"
              id="pokeSearch"
              name="pokeSearch"
              label="Search Pokemon"
              onKeyDown={this.handleKeyDown}
              onChange={this.handleChange}
              value={this.state.searchText}
              fullWidth={true} />
          </Grid>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  allPokemons: state.allPokemons
});

const mapDispatchToProps = dispatch => ({
  searchPokemons (pokemonName) {
    return dispatch(searchPokemons(pokemonName));
  },
  handlePokemons (page, pageSize) {
    return dispatch(allPokemons(page, pageSize));
  },
  resetCurrentPage () {
    dispatch(reset());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);