import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Grid, Box } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import getPokemonColors from '../../helpers/getPokemonColors';
import { allPokemons, reset } from '../../helpers/actions';

import SearchBar from '../common/SearchBar';
import Pokemons from '../Pokemons';
import Header from '../Header';

import './sass/style.sass'

class PokeIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      species: [],
      actualPage: 1,
      size: 10,
      classes: {},
      colors: [],
    }  

    this.handlePageChange = this.handlePageChange.bind(this)
  }

  componentDidMount() {
    this.props.handlePokemons(this.state.actualPage, this.state.size);
    this.PokemonColors();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      const actualPage = ( this.props.match.id - 1) * this.state.size;
      this.props.resetCurrentPage()
      this.props.handlePokemons(actualPage, this.state.size);
    }
  }

  async PokemonColors() {
    const pokemonColors = await getPokemonColors();
    this.setState({
      colors: pokemonColors,
    })
  };

  handlePageChange(event, value) {
    this.setState({
      actualPage: value,
    })
    this.props.handlePokemons(value, this.state.size)
  }

  render () {
    function useStyles() { return makeStyles((theme) => ({
        margin: {
          margin: theme.spacing(1),
        },
      }))
    };

    const classes = useStyles();
      // TODO: Pesquisa, paginação e tela detalhe ficha
      return (
        <Container maxWidth="xl">
          <div className={classes.margin}>
            <Grid container justify="space-between" alignItems="center">
              <Header />
              <SearchBar />
            </Grid>
            <Pokemons
              status={this.props.allPokemons.status}
              pokemons={this.props.allPokemons.data.pokemons}
              colors={this.state.colors} />
            <Box className="poke-index-pagination">
              <Pagination
                onChange={this.handlePageChange}
                count={this.props.allPokemons.data.total}
                siblingCount={0}
                boundaryCount={2}
                size="small" />
            </Box>
          </div>
        </Container>
      );
    }
  }

const mapStateToProps = state => ({
  allPokemons: state.allPokemons,
  getProfile: state.getProfile,
  pokemonColors: state.colors,
})

const mapDispatchToProps = dispatch => ({
  handlePokemons (page, pageSize) {
    return dispatch(allPokemons(page, pageSize));
  },
  resetCurrentPage () {
    dispatch(reset())
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(PokeIndex);