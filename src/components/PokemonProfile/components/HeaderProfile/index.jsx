import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Grid,
  Chip,
  IconButton,
} from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

const HeaderProfile = ({pokemon, color, visible}) => {
  const [ favorite, setFavorite ] = useState(false);
  const history = useHistory();

  const RenderPokemonTypes = (pokemon) => {
    if (pokemon.types && pokemon.types.length > 0) {
    return pokemon.types.map(type => (
      <Chip
        className="pokemon-profile-header-type"
        key={`${type.type.name}-${type.slot}`}
        label={ type.type.name } />))
    }
  }

  const goBack = () => {
    history.push('/');
  }

  const markAsFav = () => {
    setFavorite(!favorite)
  };


  if (visible) {
    return (
      <Grid item xs={12} className={`pokemon-profile-header ${color}`}>
        <Grid container spacing={1} alignContent="space-between">
          <Grid item xs={6} className="align-left">
            <IconButton onClick={goBack} className="pokemon-profile-header-link">
              <ArrowBackIcon />
            </IconButton>
          </Grid>
          <Grid item xs={6} className="align-right">
            {
              favorite
                ? <FavoriteIcon onClick={markAsFav} />
                : <FavoriteBorderIcon onClick={markAsFav}  />
            }
          </Grid>
        </Grid>
        <Grid container alignContent="space-between" alignItems="center">
          <Grid item xs={6} className="pokemon-profile-header-name align-left">
            <h3>{pokemon.name}</h3>
          </Grid>
          <Grid item xs={6} className="pokemon-profile-header-id align-right">
            {pokemon.id}
          </Grid>
          <Grid item xs={12} className="pokemon-profile-header-type">
            {RenderPokemonTypes(pokemon)}
          </Grid>
        </Grid>
      </Grid>
    )
  } 
  return (
    <div>
      
    </div>
  )
}

export default HeaderProfile;