import React, { useState, useEffect } from 'react'
import {
  Box,
  Paper,
} from '@material-ui/core';
import axios from 'axios';

import {
  getPokemonSpecie,
  getEvolution,
} from './services/pokemon.services'


const Evolution = ({ pokemon }) => {
  const [specie, setSpecie ] = useState(undefined);
  const [evolution, setEvolution] = useState([]);
  
  useEffect(() => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    async function setPokemonSpecie() {

      if (!specie) {
        const specie = await getPokemonSpecie(pokemon, { cancelToken: source.token } );
        setSpecie({ ...specie });
        getEvolutionChain(specie.evolution_chain.url);
      }

    }

    async function getEvolutionChain(url) {
      if (evolution.length === 0 && url) {
        const evolutionChain = await getEvolution(url, { cancelToken: source.token});
        setEvolution(evolutionChain)
      }
    }
    setPokemonSpecie();
  }, []);

  const renderEvolution = () => {
    if (evolution && evolution.length > 0 ) {
      return evolution.map((evolve, index) => (
        <div className="wrap evolution" key={`${evolve.species.name}-${index}`}>
          <div className="pokemon-profile-evolution-label">
          {`Stage ${index + 1}`}
          </div>
          <Box display="flex" alignItems="center">
            <Box xs={6} className="evolution-thumb">
              <img src={evolve.thumb} alt={evolve.species.name} />
            </Box>
            <Box xs={6} className="evolution-details">
              <h3>{evolve.species.name}</h3>
              <div className="pokemon-profile-evolution-trigger">
                {evolve.evolution_details.trigger.name}, starting at {evolve.evolution_details.min_level}
              </div>
            </Box>
          </Box>
        </div>
      ))
    }
    return ''
  }

  if (pokemon) {
    return (
      <Paper elevation={0} className="pokemon-profile-infos-container">
        <div className="pokemon-profile-infos">
          {renderEvolution()}
        </div>
      </Paper>
    )
  }
}

export default Evolution;