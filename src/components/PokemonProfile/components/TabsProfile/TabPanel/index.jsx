import React from 'react'
import {
  Box
} from '@material-ui/core';

const TabPanel = (props) => {
  const {
    children,
    value,
    index,
  } = props;

  return (
    <div role="tabpanel"
      className="pokemon-profile-infos-tabpanel"
      hidden={value !== index}
      id={`pokemon-profile-infos-tabpanel-${index}`}
      aria-labelledby={`pokemon-profile-infos-tab-${index}`}>
        { value === index && (
          <Box p={0}>
            {children}
          </Box>
        )}
    </div>
  )
}

export default TabPanel;