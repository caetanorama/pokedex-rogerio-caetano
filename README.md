# 📇 Pokedex 

Listagem de pokemons cadastrados, com visão detalhada das suas características e habilidades.

## 💻 API

As informações sobre os Pokemons serão consumidas pelo uso da API-REST [POKEAPI](https://pokeapi.co/).

## 📌 Para rodar a aplicação

No terminal, vá até a pasta onde deseja rodar a aplicação e em seguida siga os seguintes comandos no terminal:

* __git clone https://caetanorama@bitbucket.org/caetanorama/pokedex-rogerio-caetano.git__
* __cd pokedex-rogerio-caetano__
* __yarn install__ ou 
* Para rodar a aplicação: __yarn start__ 
* Para compilar a aplicação: __yarn build__

## 😄 Tecnologias utilizadas

* javascript/ReactJS
* CSS/SASS
* Material Design
* PokeAPI (https://pokeapi.co/)