import React, { useState } from 'react'
import {
  Grid,
  Container,
  Paper,
  Tabs,
  Tab
} from '@material-ui/core'

import TabPanel from './TabPanel'
import PokemonInfos from './PokemonInfos'

const TabsProfile = ({pokemon, visible}) => {
  const [activeTab, setActiveTab] = useState(0);

  const handleTabsChange = (event, newValue) => {
    setActiveTab(newValue)
  }

  if (visible) {
    return (
      <Container disableGutters maxWidth="sm" className="border-top-rounded">
        <Grid item xs={12} className={'pokemon-profile-tabs'}>
          <Paper elevation={0}>
            <Tabs
              value={activeTab}
              onChange={handleTabsChange}
              indicatorColor="primary"
              aria-label="Pokemon profile info"
              className="pokemon-profile-tabs-navigation">
                <Tab label="About"></Tab>
                <Tab label="Base Stats"></Tab>
                <Tab label="Evolution"></Tab>
              </Tabs>
              <TabPanel value={activeTab} index={0}>
                <PokemonInfos type="basic" props={pokemon} />
              </TabPanel>
              <TabPanel value={activeTab} index={1}>
                <PokemonInfos type="stats" props={pokemon} />
              </TabPanel>
              <TabPanel value={activeTab} index={2}>
                <PokemonInfos type="evolution" props={pokemon} />              
              </TabPanel>
          </Paper>
        </Grid>
      </Container>
    )
  }
  return (
    <div>
    </div>
  )
}

export default TabsProfile;