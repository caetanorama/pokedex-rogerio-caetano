import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Box,
  Backdrop,
  CircularProgress,
} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { Link } from 'react-router-dom';
import { allPokemons, reset } from '../../helpers/actions';

import './sass/styles.sass'

import PokemonDetails from './components/PokemonDetails';

class Pokemons extends Component {
  constructor(props) {
    super(props);

    this.state = ({
      ...props
    });
  }

  render() {
    const returnPokemonColor = (pokemon) => {
      const [ color ] = this.props.colors.filter(color => {
        return color.pokemon_species.some(specie => specie.name === pokemon);
      });
      if (color) return color.name;
    }

    const PokemonList = (props) => {
      const { pokemons } = props;
      if (this.props.status === 'received' && pokemons.length > 0) {
        return pokemons.map((pokemon, index) => {
          if (pokemon.name) {
            return <Grid
              item
              xs={6}
              md={4}
              key={`${pokemon.name}-${index}`}
              className="pokemon-detail-grid-container">
              <PokemonDetails pokemon={pokemon} color={returnPokemonColor(pokemon.name)} />
            </Grid>
          };
          return (
          <Backdrop open={this.props.status === 'pending'} >
            <CircularProgress />
          </Backdrop>)
        });
      }
      if (this.props.status === 'received'){
        return (
          <Grid item xs={12} className="content-not-found">
            <h3 className="header">{`Sorry, no result found :(`}</h3>
            <p className="text">The Pokemon you searched was unfortunatly not found or doesn't exist.</p>
            <Link
              to="/"
              className="link"
              onClick={this.props.handlePokemons}>
                <ArrowBackIosIcon /> Back to Home
            </Link>
          </Grid>
        )
      }

      return (
        <Backdrop open={this.props.status === 'pending'} >
          <CircularProgress />
        </Backdrop>
      )
          
    }
    // FIXME: Ajuste na altura dos boxes
    return (
      <Box className="pokemon-main"> 
        <Grid
          container
          spacing={4}
          justify='flex-start'
          alignItems='stretch'>
            <PokemonList pokemons={this.props.pokemons} colors={this.props.colors} />
        </Grid>
      </Box>
    )
  };
};

  const mapStateToProps = dispacth => ({
    allPokemons
  });

  const mapDispatchToProps = dispatch => ({
    handlePokemons (page, pageSize) {
      return dispatch(allPokemons(page, pageSize));
    },
    resetPokemons () {
      dispatch(reset())
    }
  });

export default connect(mapStateToProps, mapDispatchToProps)(Pokemons);