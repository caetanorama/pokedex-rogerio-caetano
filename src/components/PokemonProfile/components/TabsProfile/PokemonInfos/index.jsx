import React from 'react';
import {
  Typography
} from '@material-ui/core'

import Basic from './Basic'
import Stats from './Stats'
import Evolution from './Evolution';

const PokemonInfos = ({ type, props }) => {
  switch (type) {
    case 'basic':
      return (
        <Basic pokemon={props} />
      );
    case 'stats':
      return (
        <Stats pokemon={props} />
      );
    case 'evolution':
      return (
        <Evolution pokemon={props} />
      );
    default:
      return (
        <Typography align="center" color="primary" variant="h3">
          Erro na solicitação
        </Typography>
      )
  }
}

export default PokemonInfos;