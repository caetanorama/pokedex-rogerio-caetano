import React from 'react'
import {
  Box,
  Paper,
  LinearProgress,
} from '@material-ui/core';


const Stats = ({ pokemon }) => {
  
  const renderStats = () => {
    return pokemon.stats.map((stat, index) => (
      <div className="wrap" key={`${stat.stat.name}-${index}`}>
        <div className="pokemon-profile-info-label">
        {stat.stat.name.replace('-',' ')}
        </div>
        <div className="pokemon-profile-info-content">
          <Box display="flex" alignItems="center">
            <Box minWidth={25}>
              {stat.base_stat}
            </Box>
            <Box width="100%" ml={0}>
              <LinearProgress variant="determinate" value={stat.base_stat} />
            </Box>
          </Box>
        </div>
      </div>
    ))
  }

  if (pokemon) {
    return (
      <Paper elevation={0} className="pokemon-profile-infos-container">
        <div className="pokemon-profile-infos">
          {renderStats()}
        </div>
      </Paper>
    )
  }
}

export default Stats;