
  
import axios from 'axios';
import api from '../../../../../../helpers/urls';


const renderAbilites = (pokemon) => {
  if (pokemon.abilities && pokemon.abilities.length > 0) {
    return pokemon.abilities.map(ability => ability.ability.name).join(', ');
  }
  return 'undefined';
}

const getGenderRates = async (pokemonName, cancelRequest) => {
  const { data: female } = await axios.get(`${api.BASEPOINT}/gender/female`, {...cancelRequest});
  const { data: male } = await axios.get(`${api.BASEPOINT}/gender/male`, {...cancelRequest});
  const [ maleRate ] = male
    ? male.pokemon_species_details.filter((specie => specie.pokemon_species.name === pokemonName))
    : undefined
  const [ femaleRate ] = female
    ? female.pokemon_species_details.filter((specie => specie.pokemon_species.name === pokemonName))
    : undefined;
  return ({
    male: maleRate ? maleRate.rate : undefined,
    female: femaleRate ? femaleRate.rate : undefined,
  });
}

const getEffortPoints = (pokemon) => {
  const [ attack ] = pokemon.stats.filter(stat => stat.stat.name === 'special-attack')
  return `${attack.effort} ${attack.stat.name.replace('-',' ')}`;
}

const getEggGroup = async (pokemonName, cancelRequest) => {
  const eggGroups = await axios.get(`${api.BASEPOINT}/egg-group`, {...cancelRequest});
  const groups = await Promise.all(eggGroups.data.results.map(group => {
    return axios.get(group.url, {...cancelRequest}).then(res => res.data);
  }));
  const [ groupName ] = groups.filter(group => (
    group.pokemon_species
      .some(specie => specie.name === pokemonName)
    ));
  return groupName.name;
}

const getGrowthRates  = async (pokemonName, cancelRequest) => {
  const growthRates = await axios.get(`${api.BASEPOINT}/growth-rate`, {...cancelRequest});
  const listRates = await Promise.all(growthRates.data.results.map(rate => {
    return axios.get(rate.url, {...cancelRequest}).then(res => res.data);
  }));
  const [ rate ] = listRates.filter(list => (
    list.pokemon_species.some(specie => specie.name === pokemonName)
  ));
  return rate;
}

const getPokemonSpecie = async (pokemon, cancelRequest) => {
  const SpecieUrl = pokemon.species.url;
  return axios.get(SpecieUrl, {...cancelRequest}).then(res => res.data);
} 

const getEvolution = async (evolutionUrl, cancelRequest) => {
  const chain = await axios.get(evolutionUrl, {...cancelRequest});
  let resp = chain.data.chain
  const evolutionData = [];
  do {
    let [ details ] = resp['evolution_details'];

    evolutionData.push({
      species: resp.species,
      'evolution_details': details,
    });

    [ resp ] = resp['evolves_to'];
  } while (!!resp && resp.hasOwnProperty('evolves_to'));
  evolutionData.shift();
  const evolutionComplete = await Promise.all(evolutionData.map(async evolution => {
    const pokeInfo = await axios.get(`${api.BASEPOINT}/pokemon/${evolution.species.name}`);
    return ({
      ...evolution,
      "thumb": pokeInfo.data.sprites.front_default,
    })
  }));
  return evolutionComplete;
}

  export {
    renderAbilites,
    getGenderRates,
    getEffortPoints,
    getEggGroup,
    getGrowthRates,
    getPokemonSpecie,
    getEvolution,
  }