import { combineReducers } from 'redux';


import {
  ALL_POKEMONS_REQUEST,
  ALL_POKEMONS_RECEIVED,
  ALL_POKEMONS_REJECT,
  SEARCH_REQUEST,
  SEARCH_RECEIVED,
  SEARCH_REJECT,
  GET_PROFILE_REQUEST,
  GET_PROFILE_RECEIVED,
  GET_PROFILE_REJECT,
   } from './actions-types';
  
const initialState = { data: {}, status: ""};

const allPokemons = (state = initialState, action) => {
  switch (action.type) {
    case ALL_POKEMONS_REQUEST:
    case SEARCH_REQUEST:
      return Object.assign({}, state, {status: "pending"});
    case ALL_POKEMONS_RECEIVED:
    case SEARCH_RECEIVED:
      return Object.assign(
          {},
          state,
          { data: { pokemons: action.payload, total: action.total }},
          {status: "received"}
        );
    case ALL_POKEMONS_REJECT:
    case SEARCH_REJECT:
      return Object.assign({}, state, {status: "failed", error: action.payload });
    default:
      return state;
  }
}

const Profile = (state = [], action) => {
  switch (action.type) {
    case GET_PROFILE_REQUEST:
      return Object.assign({}, state, {status: "pending"});
    case GET_PROFILE_RECEIVED:
      return Object.assign(
        {},
        state,
        { data: { ...action.payload }},
        { status: "received" }
      )  
    case GET_PROFILE_REJECT:
      return Object.assign(
        {},
        state,
        { 
          status: "failed",
          error: action.payload
        }
      );
    default:
      return state;
  }
}

export default combineReducers({
  allPokemons: allPokemons,
  Profile: Profile,
});
