import axios from 'axios';
import api from './urls';
import colors from './colors';

export default () => {
  return Promise.all(colors.map(async (color) => {
    const { data: { pokemon_species }} = await axios.get(`${api.BASEPOINT}/pokemon-color/${color}`);
    return {
      name: color,
    pokemon_species
  };
  })).then(res => res);
};