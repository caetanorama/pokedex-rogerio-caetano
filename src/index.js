import React from 'react';
import ReactDom from 'react-dom';
import './index.css';
import App from './App';

import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware, createStore } from 'redux';
import * as serviceWorker from './serviceWorker';

import rootReducer from './helpers/reducers';

const Store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDom.render(
    <Provider store={Store}>
      <App />
    </Provider>,
  document.getElementById('root')
);

serviceWorker.register();