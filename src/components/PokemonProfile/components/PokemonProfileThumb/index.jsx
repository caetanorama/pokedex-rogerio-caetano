import React from 'react'

const PokemonProfileThumb = ({ pokemon, visible }) => {
  return (
    <div className="pokemon-profile-avatar-container">
      <img
        src={visible ? pokemon.sprites.front_default : ''}
        alt={pokemon.name}
        className="pokemon-profile-avatar" />
    </div>
  )
}

export default PokemonProfileThumb;