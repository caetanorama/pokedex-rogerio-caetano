import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
} from '@material-ui/core';

import './sass/style.sass'

const PokemonDetails = (props) => {
  const { pokemon, color } = props;
  const listPokemonType = (pokemon) => {
    // if (pokemon){
      return pokemon.types.map(type => {
      return <li className="pokemon-type" key={type.slot}>{ type.type.name }</li>
      });
    // }
  }

  const formatNumber = (id) => {
    return '#'+('00'+id).slice(-3);
  }

  return (
    <Fragment>
      <Link to={`/pokemon/${pokemon.name}`} props={props} className="">
        <Box className={`pokemon-detail ${color}`}>
          <div className="pokemon-detail-container">
            <div className="pokemon-detail-body">
              <p className="pokemon-name">
                { pokemon.name }
              </p>
              <ul className="pokemon-type-list">
                { listPokemonType(pokemon) }
              </ul>
              <div className="pokemon-id">
                <p>{ formatNumber(pokemon.id) }</p>
              </div>
            </div>
            <div className="pokemon-avatar">
              <img src={pokemon.sprites.front_default} alt={pokemon.name} />
            </div>
            </div>
        </Box>
      </Link>
    </Fragment>
  )
};

export default PokemonDetails;