import React from 'react';
import { BrowserRouter } from 'react-router-dom';


import Routes from './Routes';

import './App.css';
// TODO: Criar reducers para a pesquisa
function App() {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
};

export default App;
