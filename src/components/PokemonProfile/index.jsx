import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Slide,
} from '@material-ui/core';
import getPokemonColors from '../../helpers/getPokemonColors';
import { getPokemon } from '../../helpers/actions';

import HeaderProfile from './components/HeaderProfile';
import TabsProfile  from './components/TabsProfile';
import PokemonProfileThumb from './components/PokemonProfileThumb';

import './sass/styles.sass'


class PokemonProfile extends Component {
  constructor(props) {
    super(props);

    
    this.state = ({
      pokemonName: props.match.params.pokemon,
      pokemonColors: [],
    })
  }
  
  async componentDidMount() {
    this.props.handlePokemon(this.props.match.params.pokemon)
    const colors = await getPokemonColors();
    this.setState({
      pokemonColors: colors,
    })
  }

  getPokemonColor(pokemon) {
    if (this.state.pokemonColors) {
      const [ color ] = this.state.pokemonColors.filter(color => {
        return color.pokemon_species.some(specie => specie.name === pokemon);
      }
      );
      if (color) return color.name;
    }
  }


  render() {
    const pokemonProfile = this.props.profile;
    const loaded = (this.props.profile && this.props.profile.sprites) ? true : false;
    return (
      <Slide direction="left" in={loaded}>
        <Container
          disableGutters
          className={`pokemon-profile ${this.getPokemonColor(pokemonProfile.name)}`}>
          <HeaderProfile
            visible={loaded}
            pokemon={pokemonProfile}
            color={this.getPokemonColor(pokemonProfile.name)} />
          <PokemonProfileThumb pokemon={pokemonProfile} visible={loaded} />
          <TabsProfile
            visible={loaded}
            pokemon={pokemonProfile} />
        </Container>
      </Slide>
    )
  }
}

const mapStateToProps = state => ({
  profile: { ...state.Profile.data }
});

const mapDispatchToProps = dispatch => ({
  handlePokemon (pokemon) {
    return dispatch(getPokemon(pokemon))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(PokemonProfile)