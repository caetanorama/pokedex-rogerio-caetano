import React, { useState, useEffect } from 'react'
import {
  Paper, Typography
} from '@material-ui/core';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import {
  renderAbilites,
  getGenderRates,
  getEffortPoints,
  getEggGroup,
  getGrowthRates,
  getPokemonSpecie,
  getEvolution,
} from './services/pokemon.services';
import axios from 'axios';


const Basic = ({ pokemon }) => {
  const [ genderRate, setGenderRate ] = useState(undefined)
  const [ eggGroup, setEggGroupd ] = useState('');
  const [ growthRate, setGrowthRate ] = useState({});
  const [specie, setSpecie ] = useState(undefined);
  const [evolution, setEvolution] = useState([]);

  useEffect(() => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    async function getRates() {
      if (!genderRate) {
        const rates = await getGenderRates(pokemon.name, { cancelToken: source.token });
        setGenderRate({ ...rates })
      }
    }
    async function getEggsGroups() {
      if (!eggGroup) {
        const groupName = await getEggGroup(pokemon.name,  { cancelToken: source.token });
        setEggGroupd(groupName);
      }
    }
    async function getGrowth() {
      const rate = await getGrowthRates(pokemon.name,  { cancelToken: source.token });
      setGrowthRate({...rate});
    }
    async function setPokemonSpecie() {

      if (!specie) {
        const specie = await getPokemonSpecie(pokemon, { cancelToken: source.token } );
        setSpecie({ ...specie });
        getEvolutionChain(specie.evolution_chain.url);
      }

    }

    async function getEvolutionChain(url) {
      if (evolution.length === 0 && url) {
        const evolutionChain = await getEvolution(url, { cancelToken: source.token});
        setEvolution(evolutionChain)
      }
    }

    setPokemonSpecie();
    getRates();
    getEggsGroups();
    getGrowth();
    return () => {
      source.cancel();
    }
  }, [])


  if (pokemon) {
    return (
      <Paper elevation={0} className="pokemon-profile-infos-container">
        <div className="pokemon-profile-infos">
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Species
            </div>
            <div className="pokemon-profile-info-content">
              {pokemon.species ? pokemon.species.name : 'undefined'}
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Height
            </div>
            <div className="pokemon-profile-info-content">
              {pokemon.height ? pokemon.height : 'undefined'} cm
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Weight
            </div>
            <div className="pokemon-profile-info-content">
              {pokemon.weight ? pokemon.weight : 'undefined'} Kg
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Abilities
            </div>
            <div className="pokemon-profile-info-content">
              {renderAbilites(pokemon)}
            </div>
          </div>
        </div>

        <Typography variant="h5" className="pokemon-profile-info-header">
          Breeding
        </Typography>
        <div className="pokemon-profile-infos">
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Gender
            </div>
            <div className="pokemon-profile-info-content">
              <span className="gender-rate male">
                <FiberManualRecordIcon />
                {genderRate ? genderRate.male : 'undefined'}  
              </span>
              <span className="gender-rate female">
                <FiberManualRecordIcon />
               {genderRate ? genderRate.female : 'undefined'}
              </span>
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Egg Groups
            </div>
            <div className="pokemon-profile-info-content">
              {eggGroup}
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Egg Cycle
            </div>
            <div className="pokemon-profile-info-content">
              -
            </div>
          </div>
        </div>

        <Typography variant="h5" className="pokemon-profile-info-header">
          Training
        </Typography>
        <div className="pokemon-profile-infos">
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Base EXP
            </div>
            <div className="pokemon-profile-info-content">
              {pokemon.base_experience}
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Effort points
            </div>
            <div className="pokemon-profile-info-content">
              {getEffortPoints(pokemon)}
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Capture rate
            </div>
            <div className="pokemon-profile-info-content">
              {specie ? specie.capture_rate : 'undefined'}
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Base happiness
            </div>
            <div className="pokemon-profile-info-content">
              {specie ? specie.base_happiness : 'undefined'}
            </div>
          </div>
          <div className="wrap">
            <div className="pokemon-profile-info-label">
              Growth rate
            </div>
            <div className="pokemon-profile-info-content">
              {growthRate.name ? growthRate.name.replace('-',' ') : 'undefined'}
            </div>
          </div>
        </div>
      </Paper>
    )
  }
}

export default Basic;