import React from 'react';
import { Grid } from '@material-ui/core';

const Header = () => {
  return (
    <Grid item xs={12} sm={3}>
      <h2>Pokedex</h2>
    </Grid>
  );
}

export default Header;