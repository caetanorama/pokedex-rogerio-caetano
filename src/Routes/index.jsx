import React from 'react';
import { Route, Switch } from 'react-router-dom';

import PokeIndex from '../components/PokeIndex';
import PokemonProfile from '../components/PokemonProfile';

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={PokeIndex} />
      <Route path="/pokemon/:pokemon" component={PokemonProfile} />
    </Switch>
  );
};

export default Routes;