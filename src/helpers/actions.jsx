import axios from 'axios';
import api from './urls';

import {
  ALL_POKEMONS_REQUEST,
  ALL_POKEMONS_RECEIVED,
  ALL_POKEMONS_REJECT,
  SEARCH_REQUEST,
  SEARCH_RECEIVED,
  SEARCH_REJECT,
  RESET,
  GET_PROFILE_REQUEST,
  GET_PROFILE_REJECT,
  GET_PROFILE_RECEIVED
} from './actions-types';


export const allPokemons = (page, size) => (dispatch) => {
  dispatch({
    type: ALL_POKEMONS_REQUEST,
  });
  const limit = size;
  const offset = (page - 1) * size;
  let total = 0 
  axios
    .get(`${api.BASEPOINT}/pokemon-species?limit=${limit}&offset=${offset}`)
    .then(res => {
      console.log('res -> ', res);
      total = res.data.count;
      return Promise.all(res.data.results.map((poke) => {
        return axios.get(`${api.BASEPOINT}/pokemon/${poke.name}`)
          .then( result => ({
            ...poke,
            ...result
          }))
          .then(res => {
            return { ...res.data}
          })
          .catch(error => dispatch({
            type: ALL_POKEMONS_REJECT,
            payload: { error: error }
          }));
        })
      ).then(complete =>  complete)}
    )
    .then(pokemons => {
      dispatch({
      type: ALL_POKEMONS_RECEIVED,
      payload: pokemons,
      total: Math.ceil(total / limit),
    })})
    .catch(erro => dispatch({
    type: ALL_POKEMONS_REJECT,
    payload: { error: erro.error.response}
  }));
};

export const getPokemon = (pokemon) => dispatch => {
  dispatch({
    type: GET_PROFILE_REQUEST
  });
  axios.get(`${api.BASEPOINT}/pokemon/${pokemon}`)
    .then(res => res)
    .then( pokemonFound => (
      dispatch({
        type: GET_PROFILE_RECEIVED,
        payload: { ...pokemonFound.data }
      }))
    )
    .catch(error => dispatch({
      type: GET_PROFILE_REJECT,
      payload: { error: error }
    }));
}


export const searchPokemons = (pokemonName) => async dispatch => {
  dispatch({
    type: SEARCH_REQUEST
  })
  const pokemonDetails = [];
  axios.get(`${api.BASEPOINT}/pokemon/${pokemonName}`)
    .then(res => {
      pokemonDetails.push(res.data);
      dispatch({
        type: SEARCH_RECEIVED,
        payload: pokemonDetails
      })
    })
    .catch(error => dispatch({
      type: SEARCH_REJECT,
      payload: { ...error.response }
    }));
}

export const reset = () => dispatch => {
  dispatch({
    type: RESET
  });
}